package com.rinoapps.nowereklamy;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.gemius.sdk.adocean.AdStateListener;
import com.gemius.sdk.adocean.FullScreenAd;

import pl.spicymobile.mobience.sdk.MobienceSDK;

/**
 * Created by Programista on 25.05.2017.
 */

public class MainActivity extends AppCompatActivity {

    Button button;
    String INTERSTITIAL1_PLACEMENT_ID= "_2o8aad53c_f6dkptdr7UMFSoQOnzkdqFh6tjhTVyVD.p7";
    FullScreenAd interstitial1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobienceSDK.setApiKey("9Da-266h24wbFfPsB0MwlBGZ13iwIlowAYjtvjoIdn7w-s9dDlAIfDqbjB8xxA5PemuWT2Lx9yRC12I92ikIfw");
        MobienceSDK.blockDataGatheringInBackground();
        MobienceSDK.startService();
        button=(Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interstitial1.open();
            }
        });
        interstitial1 = new FullScreenAd(this,INTERSTITIAL1_PLACEMENT_ID );
        interstitial1.setNumericalVariables(MobienceSDK.getAdOceanTargeting());
        interstitial1.setAdStateListener(new AdStateListener() {
            @Override
            public void onAdReady(boolean b) {
                Log.e("Ready","Ad="+b);
            }

            @Override
            public void onFail(Throwable throwable) {
                Log.e("Fail","Ad="+throwable.getMessage());
            }

            @Override
            public void onAdClosed() {
                Log.e("Closed","Ad");
            }
        });
    }
}
