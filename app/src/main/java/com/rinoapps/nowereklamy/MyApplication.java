package com.rinoapps.nowereklamy;

import android.app.Application;

import pl.spicymobile.mobience.sdk.MobienceSDK;

/**
 * Created by Programista on 25.05.2017.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        MobienceSDK.setAppContext(this);
    }
}
